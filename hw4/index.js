let firstNumber = +prompt("Enter first number");
    while (!firstNumber || isNaN(firstNumber)) {
    firstNumber = +prompt("Enter first number", firstNumber);
    }
let secondNumber = +prompt("Enter second number");
    while (!secondNumber || isNaN(secondNumber)) {
    secondNumber = +prompt("Enter second number", secondNumber);
    }
let operation = prompt("Enter your operation (+ - * /)");

function mathOperation(a, b, c) {
    if (c == "+") {
        return (a + b);
    }
    else if (c == "-") {
        return (a - b);
    }
    else if (c == "*") {
        return (a * b);
    }
    else {
        return (a / b);
    }
}

let result = mathOperation(firstNumber, secondNumber, operation);
console.log(result);