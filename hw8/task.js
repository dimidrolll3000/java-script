function filterBy(array, criteria) {

    return array.filter( (item)=> {
  
      if (criteria === 'null' && item === null) {
        return false
      } else if ( (typeof(item) !== criteria && criteria !== null) || (criteria === 'object' && item === null) ) {
        return true
      }
  
    } )
  
  }
  
  console.log(filterBy(['hello', 'world', 23, '23', null], 'string'));